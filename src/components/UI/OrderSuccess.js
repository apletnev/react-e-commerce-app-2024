import Modal from "../UI/Modal"
import OrderSuccessImage from "../../assets/icons/order_success.svg"

const OrderSuccessModal = ({onClose}) => {
    return (
        <Modal onClose={onClose}>
            <div className="order-container">
                <div className="order-container--success">
                    <img className="img-fluid" src={OrderSuccessImage} alt="Success"/>
                    <div className="message">
                        <h1>Order successfully placed!</h1>
                        <span>OrderID #{Math.random().toString(32).slice(2)}</span>
                    </div>
                </div>
            </div>
        </Modal>
    )
}

export default OrderSuccessModal