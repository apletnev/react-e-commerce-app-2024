import { useEffect, useState } from "react"
import ListItem from "./ListItems/ListItem"
import Loader from "../UI/Loader"
import axios from "axios"

const Products = ({ onAddItem, onRemoveItem, eventState }) => {

    const [items, setItems] = useState([])
    const [loader, setLoader] = useState(true)

    useEffect(() => {

        async function fetchItems() {
            try {
                const response = await axios.get(`https://react-guide-2024-a4d9b-default-rtdb.firebaseio.com/items.json`)
                const data = response.data;
                const transfromedData = data.map((item, index) => {
                    return {
                        ...item, 
                        quantity: 0,
                        id: index
                    }
                })
                setItems(transfromedData);
            } catch (error) {
                console.log("Error: ", error);
                alert("Error occured!")
            } finally {
                setLoader(false);
            }
        }
        fetchItems();
    }, [])

    useEffect(() => {
        if (eventState.id > -1) {
            if (eventState.type === 1) {
                handleAddItem(eventState.id)
            } else if (eventState.type === -1) {
                handleRemoveItem(eventState.id)
            }
        }
    }, [eventState])

    const updateItemTitle = async (itemId) => {
        try {
            let title = `Updated title-${itemId}`;
            await axios.patch(`https://react-guide-2024-a4d9b-default-rtdb.firebaseio.com/items/${itemId}.json`, {
                title: title
            })
            let data = [...items];
            let index = data.findIndex(e => e.id === itemId);
            data[index]['title'] = title;
            setItems(data);
        } catch (error) {
            console.log(`Error updating the data: ${error}`);
        }
    }

    const handleAddItem = id => {

        let data = [...items];
        let index = data.findIndex(i => i.id === id);
        data[index].quantity += 1;

        setItems([...data]);
        onAddItem(data[index]);
    }

    const handleRemoveItem = id => {

        let data = [...items];
        let index = data.findIndex(i => i.id === id);
        if (data[index].quantity !== 0) {
            data[index].quantity -= 1;
            setItems([...data]);
            onRemoveItem(data[index]);
        }
    }

    return (
        <>
        <div className={"product-list"}>
            <div className={"product-list--wrapper"}>
                {
                    items.map(item => {
                        return (<ListItem onAdd={handleAddItem} onRemove={handleRemoveItem} key={item.id} data={item} updateItemTitle={updateItemTitle}/>)
                    })
                }
            </div>
        </div>
        {loader && <Loader/>}
        </>
    )
}

export default Products